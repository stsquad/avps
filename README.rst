.. SPDX-License-Identifier: CC-BY-SA-4.0
.. include:: macros.rst

======
README
======

This work is licensed under a
`Creative Commons Attribution-NonCommercial-ShareAlike 4.0 International License <https://creativecommons.org/licenses/by-sa/4.0/legalcode.en>`_.

:underline:`This document originates from Genivi which became` `COVESA <http://covesa.global>`_.

The resulting readable documentats in HTML and PDF are accessible from `ReadTheDocs webwite <https://automotive-virtual-platform-specification.readthedocs.io/en/latest/>`_.

To build the documentation locally, please follow the `Installing Sphinx <https://www.sphinx-doc.org/en/master/usage/installation.html>`_ instructions.

Once installed, you can run 

.. code-block:: bash
   :caption: Building the documents
  
   #build  html
   make html
  
   #build PDF
   make latexpdf

Contributions to this project are accepted under the same license
with developer sign-off as described in the :ref:`Contributing`.

