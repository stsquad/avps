.. SPDX-License-Identifier: CC-BY-SA-4.0
.. include:: ../../macros.rst
Supplemental Virtual Device categories
--------------------------------------

Text Console
^^^^^^^^^^^^

**Discussion:**

While they may be rarely an appropriate interface for the normal
operation of the automotive system, text consoles are expected to be
present for development purposes. The virtual interface of the console
is adequately defined by [VIRTIO]

Text consoles are often connected to a shell capable of running
commands. For security reasons, text consoles need to be possible to
shut off entirely in the configuration of a production system.

**AVPS Requirements**:

.. list-table:: 
  :widths: 20 80
  
  * - {AVPS-v2.0-46}
    - The virtual interface of the console MUST be
      implemented according to chapter 5.3 in [VIRTIO]
  * - {AVPS-v2.0-47}
    - To not impede efficient development, text consoles
      shall further be integrated according to the operating systems'
      normal standards so that they can be connected to any normal
      development flow.

.. list-table:: 
  :widths: 20 80
  
  * - {AVPS-v2.0-48}
    - For security reasons, text consoles MUST be possible
      to shut off entirely in the configuration of a production system.
      This configuration MUST NOT be modifiable from within any guest
      operating system.
  * - {AVPS-v2.0-49}
    - It is also recommended that technical and/or process related
      countermeasures are introduced and documented during the
      development phase, to ensure there is no way to forget to disable
      these consoles.

Filesystem virtualization
^^^^^^^^^^^^^^^^^^^^^^^^^

**Discussion**:

This chapter discusses two different features, one being host-to-vm
filesystem sharing and the other being VM-to-VM sharing, which might be
facilitated by Hypervisor functionality.

The function of providing disk access in the form of a "shared folder"
or full disk pass-through is a function that seems more used for
desktop virtualization than in the embedded systems that this document
is for. In desktop virtualization, for example the user wants to run
Microsoft Windows in combination with a MacOS host, or to run Linux in
a virtual machine on a Windows-based corporate workstation, or to try
out custom Linux systems in KVM/QEMU on a Linux host, for development
of new (e.g. embedded)
systems. Host-to-VM filesystem sharing might also serve some purpose
also in certain server virtualization setups.

The working group found little need for this host-to-vm disk sharing in
the final product in most automotive systems, but we summarize the
opportunities here if the need arises for some product.

[VIRTIO] mentions, very briefly, one network disk protocol for the
purpose of hypervisor-to-vm storage sharing, which is 9pfs. 9pfs is a
part of a set of protocols defined by the legacy Plan9 operating system.

VIRTIO is very short on details and seems to be lacking even references
to a canonical formal definition. The VIRTIO specification is thus
complemented only by scattered information found on the web regarding
the specific implementations (Xen, KVM, QEMU, ...). However, a research
paper on VirtFS however has a more explicit proposal which is also 9P
based -- see ref [9PFS]. This could be used as an agreed common basis.

9pfs is a minimalistic network file-system protocol that could be used
for simple HV-to-VM exposure of file system, where performance is not
critical.

9pfs has known performance problems however but running 9pfs over vsock
could be an optimization option. 9pfs seems to lack a flexible and
reliable security model which seems somewhat glossed over in the 9pfs
description: It briefly references only "fixed user" or "pass-through"
for mapping ownership on files in guest/host.

A more advanced network disk protocol such as NFS, SMB/SAMBA would be
too heavy to implement in the HV-VM boundary, but larger guest systems
(like a full Linux system) can implement them within the normal
operating system environment that is running in the VM. Thus, the
combined system could likely use this to share storage between VMs over
the (virtual) network and in that case the hypervisor/virtual platform
does not need an explicit implementation.

A recently proposed VIRTIO-fs [VIRTIO-FS] aims to “provide local file
system semantics between multiple virtual machines sharing a directory
tree”. It uses the FUSE protocol over VIRTIO, which means reusing a
proven and stable interface and guarantees the expected POSIX filesystem
semantics also when multiple VMs operate on the same file system.

Optimizations of VM-to-VM sharing will be possible by using shared
memory as being defined in VIRTIO 1.2. File system operations on data
that is cached in memory will then be very fast also between VMs.

As stated, it is uncertain if fundamental host-to-vm file system sharing
is a needed feature in typical automotive end products, but the new
capabilities might open up a desire to use this to solve use-cases that
were previously not considering shared filesystem as the mechanism. We
can envision something like software update use-cases that have the
Hypervisor in charge of modifying the actual storage areas for code. For
this use case, download of software might still happen in a VM which has
advanced capabilities for networking and other operations, but once the
data is shared with the HV, it could take over the responsibility to
check software authenticity (after locking VM access to the data of
course) and performing the actual update.

In the end, using filesystem sharing is an open design choice since the
data exchange between VM and HV could alternatively be handled by a
different dedicated protocol.

References:

- VIRTIO 1.0 spec : {PCI-9P, 9P device type}.
- Kernel support: Xen/Linux 4.12+ FE driver Xen implementation details
- [VIRTIO-FS] (see reference section)
- The VirtFS paper [9PFS]

Some other 9pfs-related references include:

- A set of man pages that seem to be the definition of P9.
- QEMU instructions how to set up a VirtFS (P9).
- Example/info how to natively mount a 9P network filesystem.
- Source code for 9pfs FUSE driver
- The VirtFS paper [9PFS]

.. note:: This is mostly to indicate the scattered nature of 9P specification.
  Links are not provided since we cannot now evaluate the completeness,
  or if these should be considered official specification or not).*

**AVPS Requirements**:

.. list-table:: 
  :widths: 20 80
  
  * - {AVPS-v2.0-50} 
    - If filesystem virtualization is implemented, then
      [VIRTIO-FS] MUST be one of the supported choices.