.. SPDX-License-Identifier: CC-BY-SA-4.0
.. include:: ../../macros.rst
GPIO
----

**Discussion**:

GPIOs are typically simple devices that consist of a set of pins that
can be operated in input or output mode. Each pin can be either on or
off, sensing a state in input mode, or driving a state in output mode.
For example, GPIOs can be used for sensing buttons and switching, or
driving LEDs or even communication protocols. Hardware-wise a set of
pins forms a GPIO block that is handled by a GPIO controller.

In a virtualization setup, a guest might want to control a whole GPIO
block or just single pins. For a GPIO block that is provided by a GPIO
controller, the hypervisor can pass-through the controller so that the
guest can directly use the device with its appropriate drivers. If pins
on a single GPIO block shall be shared across multiple guests, or a
guest shall not have access to all pins of a block, the hypervisor must
multiplex access to this block. Since GPIO blocks are rather simple
devices, the platform specification recommends emulating a widely used
GPIO block and use the unmodified drivers already existing in common
operating systems.

Usage of GPIOs for time-sensitive use, such as “bit-banging”, is not
recommended because it requires a particular scheduling of the guest.
For such cases, the virtual platform should provide other suitable means
to implement a driver for the functionality that is being emulated by
bit-banging.

Future Outlook:

There is a proposal described in the ACRN project for VIRTIO transport
that implementations may consider. If the support becomes officially
proposed in the VIRTIO specification and Linux mainline drivers appear
then this may be considered as a platform requirement in a later
version. There is an independently proposed Linux driver to consider .

**AVPS Requirements**:

.. list-table:: 
  :widths: 20 80
  
  * - {AVPS-v2.0-37}
    - The hypervisor/equivalent shall support configurable
      pass-through access to a VM for digital general-purpose I/O hardware
  * - {AVPS-v2.0-38}
    - The platform may provide emulation of a widely used
      GPIO block which already has drivers in Linux and other kernels

.. todo:: A future specification version may require a specific emulation API
  (e.g. VIRTIO, when it exists) for better portability. Potential for
  future work exists here.
