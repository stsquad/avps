.. SPDX-License-Identifier: CC-BY-SA-4.0
.. include:: ../../macros.rst
Audio
-----

**Discussion**:

There is a pending proposal to the next VIRTIO specification for
defining the audio interface. It includes how the Hypervisor can report
audio capabilities to the guest, such as input/output
(microphone/speaker) capabilities and what data formats are supported
when sending audio streams.

Sampled data is expected to be in PCM format, but the details are
defined such as resolution (number of bits), sampling rate (frame rate)
and the number of available audio channels and so on.

Most such capabilities are defined independently for each stream. One VM
can open multiple audio streams towards the Hypervisor. A stream can
include more than one channel (interleaved data, according to previous
agreement of format).

Noteworthy is that this virtual audio card definition does not support
any controls (yet). For example, there is no volume control in the
VIRTIO interface, so each guest basically does nothing with volume and
mixing/priority is somehow implemented by Hypervisor layer (or companion
VM, or external amplifier, or…) or software control (scaling) of volume
would have to be done in the guest VM through user-space code doing
this.

It might be a good idea to define a Control API to set
volume/mixing/other on the hypervisor side. In a typical ECU, the volume
mixing/control might be implemented on a separate chip, so the actual
solutions vary.

Challenges include the real-time behavior, keeping low latency in the
transfer, avoiding buffer underruns, etc. Determined reliability may
also be required by some safety-critical audio functions and the
separation of audio with varying criticality is required, although
sometimes this is handled by preloading
chimes/sounds into some media hardware and triggered through another
event interface.

State transitions can be fed into the stream. Start, Stop, Pause,
Unpause. These transitions can trigger actions. For example, when
navigation starts playing, you can lower the volume of media.

Start means start playing the samples from the buffer (which was earlier
filled with data) (and opposite for input case). Pause means stop at the
current location, do not reset internal state, so that unpause can
continue playing at that location.

There are no events from the virtual hardware to the guest because it
does not control anything. It is also not possible to be informed about
buffer underrun, etc.

A driver proof of concept exists in the OpenSynergy GitHub, and an
example implementation in QEMU already. Previously QEMU played audio by
hardware emulation of a sound card, whereas this new approach is using
VIRTIO.

**Potential future requirements:**

.. todo:: Assign audio requirement
    
.. list-table:: 
  :widths: 20 80
  
  * - [PENDING]
    - If virtualized audio is implemented it MUST implement the VIRTIO-sound standard according to [VIRTIO-SND].

There are several settings / feature flags that should be evaluated to
see which ones shall be mandatory required on an automotive platform.

