.. SPDX-License-Identifier: CC-BY-SA-4.0

==========
References
==========

.. _references:

.. list-table:: 
  :widths: 25 75
  :class: longtable

  * - **[9PFS]**
    - `9PFS - A virtualization aware File System pass-through <https://www.researchgate.net/publication/228569314_VirtFS--A_virtualization_aware_File_System_pass-through>`_
  * - **[DeviceTree-Chosen]** 
    - `How to pass explicit data from firmware to OS, e.g. kaslr-seed <https://github.com/devicetree-org/dt-schema/blob/main/dtschema/schemas/chosen.yaml>`_
  
      | `Linux specifics <https://elixir.bootlin.com/linux/latest/source/Documentation/devicetree/usage-model.rst>`_
  * - **[RFC 2119]** 
    - `RFC2119.txt <https://www.ietf.org/rfc/rfc2119.txt>`_
  * - **[RPMB]**
    -  `OPTEE description <https://xen-troops.github.io/papers/optee-virt-rpmb.pdf>`_
       `Linaro information <https://lists.linaro.org/pipermail/tee-dev/2020-January/001413.html>`_
  * - **[SBSA]**
    - `Server Base System Architecture 7.0 <https://developer.arm.com/docs/den0029/latest>`_
  * - **[SCMI]**
    - `System Control and Management Interface, v 2.0 or later if not otherwise specified <http://infocenter.arm.com/help/index.jsp?topic=/com.arm.doc.den0056b/index.html>`_
  * - **[SCMI3]**
    - `System Control and Management Interface, v3.0 <https://developer.arm.com/documentation/den0056/c/?lang=en>`_
  * - **[SCMI-IIO]**
    - `SCMI Linux kernel driver <https://lwn.net/Articles/844752/>`_
  * - **[VIRTIO]**
    - `OASIS Virtual I/O Device (VIRTIO) Version 1.1, Committee Specification 01, released 11 April 2019 <https://docs.oasis-open.org/VIRTIO/VIRTIO/v1.1/VIRTIO-v1.1.html>`_
  * - **[VIRTIO-CRYPTO]**
    - `OASIS Virtio Crypto chapter <https://docs.oasis-open.org/VIRTIO/VIRTIO/v1.1/cs01/VIRTIO-v1.1-cs01.html#x1-3500009>`_
  * - **[VIRTIO-FS]**
    - `Virtio FS on GitLab <https://VIRTIO-fs.gitlab.io/>`_
  * - **[VIRTIO-GPU]**
    - Chapter 5.7 GPU-Device in VIRTIO 1.1
  * - **[VIRTIO-IOMMU]**
    - `Chapter 5.13 (proposed in the following rendering of VIRTIO:) <http://jpbrucker.net/VIRTIO-iommu/spec/VIRTIO-v1.1+VIRTIO-iommu.pdf#1a8>`_
      with the separate description `here <https://jpbrucker.net/VIRTIO-iommu/spec/VIRTIO-iommu-v0.13.pdf>`_
  * - **[VIRTIO-SCMI]**
    - `OASIS VIRTIO-SCMI chapter (future VIRTIO release) <https://github.com/oasis-tcs/VIRTIO-spec/commit/80b54cfd10a3e3d40eef532e7741ec50e63618b8>`_
      `In tex <https://github.com/oasis-tcs/VIRTIO-spec/blob/master/VIRTIO-scmi.tex>`_
  * - **[VIRTIO-SND]**
    - `OASIS Virtio-SND chapter, likely to be part of VIRTIO 1.2 - currently merged to the master branch <https://github.com/oasis-tcs/VIRTIO-spec/blob/master/VIRTIO-sound.tex>`_
  * - **[VIRTIO-VIRGL]**
    - `non OASIS VIRTIO-VIRGL description <https://github.com/Keenuts/VIRTIO-GPU-documentation/blob/master/src/VIRTIO-GPU.md>`_
  * - **[VIRTIO-VULKAN]**
    -  `Freedesktop Vulkan description <https://gitlab.freedesktop.org/virgl/virglrenderer/-/milestones/2>`_
